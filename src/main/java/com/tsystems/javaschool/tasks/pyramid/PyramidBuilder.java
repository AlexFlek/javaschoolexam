package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int k = countLines(inputNumbers);
        for (Integer i : inputNumbers) {
            if (i == null) {
                throw new CannotBuildPyramidException();
            }
        }

        Collections.sort(inputNumbers);
        Integer[] arr = inputNumbers.toArray(new Integer[inputNumbers.size()]);

        int result[][] = new int[k][2 * k - 1];
        int pos = 0;
        for (int i = 0; i < k; i++) {
            int a = 0;
            for (int j = pos; j < pos + i + 1; j++) {

                result[i][k - i + a - 1] = arr[j];
                a += 2;
            }
            pos = pos + i + 1;
        }

        return result;
    }

    int countLines(List<Integer> list) {
        int l = list.size();
        double n = (-1 + Math.sqrt(1 + 8 * l)) / 2;
        if (n % 1 != 0) {
            throw new CannotBuildPyramidException();
        }
        return (int) n;
    }
}
