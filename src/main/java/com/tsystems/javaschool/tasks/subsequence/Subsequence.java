package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        if (x.size() > y.size()) {
            return false;
        }

        Object[] arrx = x.toArray();
        Object[] arry = y.toArray();

        int sub = 0;

        for (int i = 0; i < arrx.length; i++) {
            for (int j = sub; j < arry.length; j++) {
                if ((arrx.length - i) > (arry.length - sub)) {
                    return false;
                }
                if (isEq(arrx[i], (arry[j]))) {
                    sub = j + 1;
                    break;
                }
                if (j == arry.length - 1 && !arrx[i].equals(arry[j])) {
                    return false;
                }
            }
        }
        return true;
    }

    boolean isEq(Object o1, Object o2) {
        return o1 != null ? o1.equals(o2) : o2 == null;
    }

}