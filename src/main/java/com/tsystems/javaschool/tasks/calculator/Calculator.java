package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;


public class Calculator {


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        ScriptEngineManager sem = new ScriptEngineManager();
        ScriptEngine se = sem.getEngineByName("JavaScript");
        try {
            if (statement == null){
                return null;
            }
            if (statement.contains(",") || statement.contains("//")){
               return null;
            }
            final Object eval = se.eval(statement);
            if (isNumeric(eval)){
                return eval.toString();
            } else {
                return null;
            }
        } catch(ScriptException e) {
            return null;
        }
    }

    private boolean isNumeric(Object eval) {
        if (eval instanceof Number){
            if (eval instanceof Double && !Double.isFinite((Double) eval)){
                return false;
            }
            return true;
        } else if (eval instanceof String){
            return eval != null && ((String) eval).matches("[+-]?\\d*\\.?\\d+");
        }
        return false;

    }

}